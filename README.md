# eslint-config-epscloud

-   Create a .eslintrc file with the following content:

```json
{
    "extends": "epscloud"
}
```

-   Add all the peerDependencies from this repository to your devDependencies. (i.e. eslint, eslint-plugin-import, etc)
-   Add eslint-config-epscloud to the devDependencies (replace TAG with the preferred version):

```json
{
    "devDependencies": {
        "eslint-config-epscloud": "git+https://bitbucket.org/focuspro-peps/eslint-config-epscloud.git#TAG"
    }
}
```
