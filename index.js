module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
        jest: true,
    },
    extends: [
        'airbnb-typescript',
        'airbnb/hooks',
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'plugin:eslint-comments/recommended',
        'plugin:promise/recommended',
        'plugin:jest/recommended',
        'plugin:jest/style',
        'prettier',
        'plugin:prettier/recommended',
        'plugin:security/recommended',
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: './tsconfig.json',
        tsconfigRootDir: '.',
        ecmaVersion: 2020,
        sourceType: 'module',
    },
    plugins: [
        'react-hooks',
        '@typescript-eslint',
        'prettier',
        'import',
        'promise',
        'jest',
        'security',
        'no-secrets',
    ],
    settings: {
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx'],
        },
        'import/resolver': {
            typescript: {
                alwaysTryTypes: true,
            },
        },
        react: {
            version: '16.14.4',
        },
    },
    rules: {
        // OUR ADDITIONAL RULES
        // Prettier. We use only warnings for the sake of less "unnecessary red lines". The build will still fail if there are prettier errors.
        'prettier/prettier': ['warn'],

        // See https://github.com/nickdeis/eslint-plugin-no-secrets
        'no-secrets/no-secrets': 'error',

        'import/no-unresolved': 'error',

        // See https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/array-type.md
        '@typescript-eslint/adjacent-overload-signatures': 'warn',

        // See https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/array-type.md
        '@typescript-eslint/array-type': ['error', { default: 'array-simple' }],

        '@typescript-eslint/ban-tslint-comment': 'error',

        '@typescript-eslint/class-literal-property-style': 'error',

        // See https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/explicit-function-return-type.md
        // TODO enable
        // '@typescript-eslint/explicit-function-return-type': [
        //     'warn',
        //     { allowExpressions: true },
        // ],

        // Prefer optional chaining. See https://medium.com/inside-rimeto/optional-chaining-in-typescript-622c3121f99b
        '@typescript-eslint/prefer-optional-chain': 'error',

        // RULES WHICH WE TURN OFF
        // We prefer no default over default exports since this is a better practice.
        'import/prefer-default-export': 'off',
        // Waiting for respective typescript rule https://github.com/typescript-eslint/typescript-eslint/issues/1103
        'class-methods-use-this': 'off',

        // Reassigning props is a general use case therefore we don't use this rule on props.
        'no-param-reassign': ['error', { props: false }],

        // TODO re-enable this rule
        '@typescript-eslint/prefer-regexp-exec': 'off',

        // TODO We need to reconsider our implementation (input forms) in general if we want to enable those rules. Form values we defined (constant keys) are currently posted together with user-defined placeholders (non-constant keys) which is not ideal.
        // https://github.com/nodesecurity/eslint-plugin-security/blob/master/docs/the-dangers-of-square-bracket-notation.md
        'security/detect-object-injection': 'off',

        // TODO re-enable this rule
        'promise/always-return': 'off',

        'no-console': [
            'warn',
            {
                allow: ['error'],
            },
        ],

        'react/jsx-filename-extension': 'off',
    },
};
